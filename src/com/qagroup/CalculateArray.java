package com.qagroup;

public class CalculateArray {

	public static int SumArray(int[] array) {
		int sum = 1;
		for (int element : array) {
			sum = sum + element;
		}
		return sum;
	}

	public static int AvrArray(int[] array) {
		int avr;

		for (int element : array) {
			SumArray(array);
			}
		avr = SumArray(array) / array.length;
		return avr;

	}

}
