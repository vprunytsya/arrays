package com.qagroup;

public class Car {

	private static int counter = 0;
	private String model;
	private static String color;
	private String currentSpeed;
	
	public  Car(String whichModel, String whichColor) {
		
	    counter++;
		this.model=whichModel;
		this.color=whichColor;
	}
	
	public static int numberofCreatedCars(){
		return counter;
		
	}
	public String getModel() {
		return this.model;
	}

	public String getColor() {
		return this.color;
	}

	public static String changeColor(String newColor) {
		return color = newColor;
	}

	public void info() {
		System.out.println("Model: " + model + "; Color: " + color + "; Current Speed: " + currentSpeed);

	}

}