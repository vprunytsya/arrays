package com.qagroup;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class CarTestRunner {

	@Test
	public void carNumber() {
		Car car1 = new Car("Kia", "Red");
		Car car2 = new Car("Nissan", "Black");
		Car car3 = new Car("Audi", "Green");
		int expectedResult = 3;

		int actualResult = Car.numberofCreatedCars();
		assertEquals(actualResult, expectedResult);

	}

	@Test
	public void carModel() {
		Car car = new Car("Nissan", "Black");
		String expectedResult = "Nissan";

		String actualResult = car.getModel();
		assertEquals(actualResult, expectedResult);
	}

	@Test
	public void carColor() {
		Car car = new Car("Audi", "Green");
		String expectedResult = "Green";

		String actualResult = car.getColor();
		assertEquals(actualResult, expectedResult);
	}

	@Test
	public void carChangeColor() {
		Car car = new Car("Bugatti", "Yellow");
		String expectedResult = "Coral";
		Car.changeColor("Coral");

		String actualResult = car.getColor();
		assertEquals(actualResult, expectedResult);

	}
}
